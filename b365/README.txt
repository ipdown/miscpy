Tested on Arch Linux x64

1. Install firefox, geckodriver, virtualenv, python3x, git
    (sudo pacman -S firefox geckodriver python-virtualenv python git)

2. Create new virtualenv:
    virtualenv anyname
    source anyname/bin/activate

3. From inside virtualenv install needed packages:
    pip install -r requirements.txt

4. Now you should be able it:
   python stage1.py

5. deactivate


################################################################################
# Compiling
1. type make build; NOTE: typing just make will compile and run automatically

2. cd dist ; The following files should be here:
	main.py
	stage1.cpython-38-x86_64-linux-gnu.so
	stage2.cpython-38-x86_64-linux-gnu.so

3. source ~/b365.env/bin/activate && ./main.py
