# $Id$
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from collections import OrderedDict as odict
from datetime import datetime, timedelta
from pytz import timezone
import re
import stage2 as mysql


class Bet365:
    def __init__(self, *args, **kwargs):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.driver.implicitly_wait(10)

    def  __enter__(self):
        self.driver.get("https://bet365.com/")
        return self

    def __exit__(self, type, val, traceback):
        print('Exiting.')
        self.driver.close()

    def wait(self, timeout = 600):

        try:
            print('Waiting {} seconds..'.format(timeout))
            WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located((By.TAG_NAME, 'no_such_tag')))
        except:
            print('Done, wait finished.')

    def clickElement(self, sel, text = None):
        for item in self.driver.find_elements_by_css_selector(sel):
            if text is None:
                item.click()
                return True
            elif text == item.text:
                item.click()
                return True
        return False

    def cssSelItems(self, sel):
        return self.driver.find_elements_by_css_selector(sel)


    def getLeagueDivisionName(self, item):
        return item.find_element_by_css_selector('div.suf-CompetitionMarketGroupButton_Text').text

    def getEventTime(self, item):
        try: return item.find_element_by_css_selector('div.sgl-ParticipantFixtureDetails_BookCloses').text
        except NoSuchElementException as e:
            pass
        return None

    def getEventDate(self, item, tz_srv, tz_cet):

        sdate = self.getEventTime(item)
        if sdate is not None:
            evtDate = None
            wday,time = sdate.split()
            utc  = datetime.utcnow()
            for hour in range(13):
                utc_ts = utc + timedelta(hours=hour)
                loc_ts = utc_ts.replace().astimezone(tz_srv)
                if wday in loc_ts.strftime('%a'):
                    ms = loc_ts.strftime('%Y-%m-%d {}'.format(sdate))
                    evtDate = datetime.strptime(ms, '%Y-%m-%d %a %H:%M')
                    break
            # should not happen
            if evtDate is None:
                evtDate = datetime.strptime(sdate, '%a %H:%M')

            return evtDate.replace(tzinfo = tz_srv).astimezone(tz_cet)
        return None

    def getTeamNames(self, item):
        for team in item.find_elements_by_css_selector('div.sgl-ParticipantFixtureDetails_Team'):
            yield team.text

    def getServerTime(self):
        try: return self.driver.find_element_by_css_selector('div.fm-FooterModule_ServerTime').text
        except: return datetime.now(timezone('EET')).strftime('Server Time %H:%M:%S EET')

    def getServerTZ(self):
        tz_srv = 'EET'
        srv_time = self.getServerTime()
        try:
            tz_srv = re.match('Server Time \d+:\d+:\d+ ([A-Z]+)', srv_time).group(1)
        except:
            pass
        finally:
            return timezone(tz_srv)


class Event:
    def __init__(self, date, teams):
        self.date = date
        self.team1 = teams[0]
        self.team2 = teams[1]
        self.odds = {
            '1' : '0',
            'X' : '0',
            '2' : '0',
            'D' : '0',
        }

    def __str__(self):
        return '{}-{}:{}-{},{},{},{}'.format(
                self.date, self.team1, self.team2,
                self.odds['1'], self.odds['X'], self.odds['2'], self.odds['D'])

def main():

    sel = [
        ('div.fm-Menu_Language', None),
        ('div.fm-LanguageDropDownItem', 'English'),
        ('div.wn-PreMatchItem','Soccer'),
        ('div.sm-UpComingFixturesMultipleParticipants_Region', 'Next 12 Hours'),
    ]

    with Bet365() as bet:
        while True:
            try:
                if all([bet.clickElement(*item) for item in sel]):
                    print('OK')


                    # for some reason we have to select all those
                    for i, item in enumerate(bet.cssSelItems('gl-MarketGrid')):
                        pass

                    tz_srv = bet.getServerTZ()
                    tz_cet = timezone('CET')

                    # first expand any collapsed
                    for btn in bet.cssSelItems('div.suf-CompetitionMarketGroup-collapsed'):
                        btn.click()


                    leagues = odict()
                    for i, item in enumerate(bet.cssSelItems('div.suf-CompetitionMarketGroup')):

                        league = bet.getLeagueDivisionName(item)

                        events = [Event(bet.getEventDate(lhs,tz_srv,tz_cet), list(bet.getTeamNames(lhs)))
                                    for lhs in item.find_elements_by_css_selector('div.sgl-ParticipantFixtureDetails_LhsContainer')]

                        for i, other in enumerate(item.find_elements_by_css_selector('div.sgl-ParticipantFixtureLink.gl-Market_General-cn1')):
                            events[i].odds['D'] = other.text

                        for i, colhdr in enumerate(item.find_elements_by_css_selector('div.suf-MarketOddsExpand.gl-Market_General.gl-Market_General-columnheader')):
                            label = colhdr.find_element_by_css_selector('div.suf-MarketColumnHeader_Label').text
                            for idx, val in enumerate(colhdr.find_elements_by_css_selector('div.sgl-ParticipantOddsOnly80')):
                                events[idx].odds[label] = val.text

                        leagues[league] = events

                    for name,evts in leagues.items():
                        print('LEAGUE: %s' % name)
                        for evt in evts:
                            print('EVT: %s' % evt)
                        print(80*'-')

                    dry_run = False
                    db = mysql.connect()
                    if db:
                        print('Connected to DB..')
                        mysql.insert(db, leagues, dry_run)
                    else:
                        print('No DB connection')

                    bet.wait()
            except KeyboardInterrupt:
                break


if __name__ == '__main__':
    main()

# vim:ts=4:sw=4:cc=80:et
