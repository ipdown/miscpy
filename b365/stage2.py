# $Id$
# -*- coding: utf-8 -*-
import mysql.connector
import hashlib
import datetime
import re
import configparser

def getConfig():
    cfg = configparser.ConfigParser()
    if cfg.read('db.cfg'):
        if 'mysql' in cfg.sections():
            return dict(cfg['mysql'])
    return dict()

def connect():
    cfg = getConfig()
    return mysql.connector.connect( **getConfig())

def insert(db, data, dry_run=True):
    cursor = db.cursor()

    fields = 'mainkey, matchstart, sport, liga, nameT1, nameT2, g1, gX, g2'
    for liga,evts in data.items():
        for evt in evts:

            if evt.date is None: # probably has started XXX
                continue

            mkey = '{}-{}-{}-{}'.format(liga, evt.team1, evt.team2, evt.date.strftime('%Y%m%d%H%M')).encode('utf-8')
            mainkey = hashlib.sha256(mkey).hexdigest()
            matchstart = evt.date.strftime('%Y-%m-%d %H:%M:%S')

            sport = '1' # Soccer

            nameT1 = evt.team1
            nameT2 = evt.team2
            g1     = evt.odds['1'].strip()
            gX     = evt.odds['X'].strip()
            g2     = evt.odds['2'].strip()

            if not g1: g1 = '0'
            if not gX: gX = '0'
            if not g2: g2 = '0'

            vals = (mainkey, matchstart, sport, liga, nameT1, nameT2, g1, gX, g2)
            sql = 'INSERT INTO matchdatatbl ({}) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)'.format(fields)
            print(sql % vals)

            if not dry_run:
                try:
                    cursor.execute(sql, vals)
                except mysql.connector.errors.IntegrityError:
                    pass
                except mysql.connector.errors.DataError: # XXX TODO
                    pass

    if not dry_run:
        db.commit()



if __name__ == '__main__':

    db = connect()
    cursor = db.cursor()

    cursor.execute('SHOW TABLES')
    for table in cursor:
        print(table)

    cursor.execute('SELECT * FROM matchdatatbl')
    print(cursor.fetchone())

# vim:ts=4:sw=4:cc=80:et
